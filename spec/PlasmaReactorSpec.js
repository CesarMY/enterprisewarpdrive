describe("PlasmaReactor", function () {
  var plasmaReactor;

  var testCases = [
    [[0, 0, 0], 100, [100, 100, 100], -1],
    [[0, 0, 0], 90, [90, 90, 90], -1],
    [[0, 0, 0], 30, [30, 30, 30], -1],
    [[20, 10, 0], 100, [90, 100, 110], 5400],
    [[0, 0, 100], 80, [120, 120, 0], 4800],
    [[0, 0, 0], 150, [150, 150, 150], 3000],
    [[0, 0, 30], 140, [150, 150, 120], 3000]
  ];

  beforeEach(function () {
    plasmaReactor = new PlasmaReactor(plasmaFlow, numberOfInjectors);
  });

  it("should be able to initialize the injectors", function () {
    expect(plasmaReactor.injectors.length).toEqual(numberOfInjectors);
  });

  function testPlasmaReactor (index, testCase) {
    it("should be able to get each injector optimized flow for test case " + (index + 1) + " -- Injector damage: " + testCase[0] + ", Speed of light percentage: " + testCase[1], function () {
      var injectorsDamage = testCase[0];
      var injectorsOptimizedFlow = plasmaReactor.getInjectorsFlow(testCase[1], injectorsDamage);
      expect(plasmaReactor.speedOfLightPercentage).toEqual(testCase[1]);
      for (var i = 0; i < injectorsOptimizedFlow.length; i++) {
        expect(injectorsOptimizedFlow[i][0]).toEqual(testCase[2][i]);
      }
      expect(plasmaReactor.workingTime).toEqual(testCase[3]);
    });
  }

  for (var i = 0; i < testCases.length; i++) {
    testPlasmaReactor(i, testCases[i]);
  }

  it("should be able to get each injector optimized flow for test case 8: [20, 50, 40, 170]", function () {
    var injectorsDamage = [20, 50, 40];
    expect(function () {
      plasmaReactor.getInjectorsFlow(170, injectorsDamage);
    }).toThrowError("[PlasmaReactor::getInjectorsFlow] Unable to comply");
  });
});
