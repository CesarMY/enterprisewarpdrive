describe("Injector", function () {
  var injector;

  beforeEach(function () {
    injector = new Injector();
  });

  it("should be able to calculate plasma flow", function () {
    injector = new Injector(maxFlow, maxOverrideFlow, overrideSecondsWorkLoss, 0);
    expect(injector.plasmaFlow()).toEqual(maxFlow);
  });

  it("should be able to calculate the maximum overide flow", function () {
    injector = new Injector(maxFlow, maxOverrideFlow, overrideSecondsWorkLoss, 0);
    expect(injector.overrideMaxFlow()).toEqual(maxFlow + maxOverrideFlow);
  });

  it("the maximum working time for " + maxFlow + "mg/s should be infinite", function () {
    injector = new Injector(maxFlow, maxOverrideFlow, overrideSecondsWorkLoss, 0);
    expect(injector.overrideMaxTime(maxFlow)).toEqual(-1);
  });

  it("the maximum working time for " + (maxFlow + 1) + "mg/s should be " + overrideSecondsWorkLoss + " seconds", function () {
    injector = new Injector(maxFlow, maxOverrideFlow, overrideSecondsWorkLoss, 0);
    expect(injector.overrideMaxTime(maxFlow + 1)).toEqual(overrideSecondsWorkLoss * maxOverrideFlow);
  });

  it("the maximum working time for " + (maxFlow + maxOverrideFlow) + "mg/s should be " + (overrideSecondsWorkLoss * maxOverrideFlow) + " seconds", function () {
    injector = new Injector(maxFlow, maxOverrideFlow, overrideSecondsWorkLoss, 0);
    expect(injector.overrideMaxTime(maxFlow + maxOverrideFlow)).toEqual(overrideSecondsWorkLoss);
  });
});