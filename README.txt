Enterprise Warp Drive Manager
================

Created by: Cesar Montalvo
Date: 2017-10-15

Folder structure
================
-- enterpriseWarpDrive
---- lib
------ jasmine-2.8.0            Jasmine test suit
---- spec                       Individual test specifications for each application object
------ InjectorSpec.js
------ PlasmaReactorSpec.js
---- src                        Application objects
------ Config.js
------ Injector.js
------ PlasmaReactor.js
---- SpecRunner.html            Launches the test suit for the project

App comments
================
The Enterprise Warp Drive Manager is divided in two classes.

Injector:
    This class represents one of the injectors of the plasma reactor.
    Properties:
        maxFlow                         The maximum amount in mg/s that the injector can push to the reactor
        maxOverrideFlow                 The maximum override amount in mg/s that the injector can push to the reactor
        overrideSecondsWorkLoss         The time lost when overriding the injector flow
        damage                          The the damage that the injector had received
    Methods:
        plasmaFlow                      Calculate the available plasma flow that the injector can push to the reactor
        overrideMaxFlow                 Calculate the override injector flow
        overrideMaxTime                 Calculate the maximum working time of the injector when overridden

PlasmaReactor:
    This class represents the plasma reactor. The plasma reactor has as many injectors as defined in the Config.js file.
    Properties:
        plasmaFlow                      The maximum the reactor needs to achieve 100% of the speed of light. Can be modified on the Config.js file
        numberOfInjectors               The number of injectors that the reactor has. Can be modified on the Config.js file
        injectors                       Array of injector instances.
        workingTime                     The maximum amount of seconds that the reactor can work
        speedOfLightPercentage          The actual percentage of speed light that the reactor is working
    Methods:
        initializeInjectors             Initializes the injectors, called from on the constructor
        getTotalDamage                  Calculates the total damage of the injectors
        getAvailableInjectors           Calculate how many injectors are still functional
        getInjectorDamageCompensation   Calculate a special compensation for the flow calculation
        updateSpeedPercentage           Update the actual speed of light percentage where the reactor is working
        updateWorkingTime               Update the maximum amount of working time of the reactor
        getInjectorsFlow                Calculates the optimized push flow for each injector

Notes
================
- In order to execute the test just open the SpecRunner.html with an internet browser.
- To change the plasma reactor configuration update the settings in Config.js