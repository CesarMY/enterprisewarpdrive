function Injector (maxFlow, maxOverrideFlow, overrideSecondsWorkLoss, injectorDamage) {
  this.maxFlow = maxFlow;
  this.maxOverrideFlow = maxOverrideFlow;
  this.overrideSecondsWorkLoss = overrideSecondsWorkLoss;
  this.damage = injectorDamage;
}

Injector.prototype.plasmaFlow = function () {
  if (this.damage === 100) {
    return 0;
  }

  return this.maxFlow - this.damage;
};

Injector.prototype.overrideMaxFlow = function () {
  if (this.damage === 100) {
    return 0;
  }

  return this.plasmaFlow() + this.maxOverrideFlow;
};

Injector.prototype.overrideMaxTime = function (usedFlow) {
  var plasmaFlow = this.plasmaFlow();
  var usedOverrideFlow = usedFlow - plasmaFlow;

  if (usedFlow === 0) {
    return -2;
  }

  if (usedOverrideFlow > 0 && usedOverrideFlow <= this.maxOverrideFlow) {
    return this.overrideSecondsWorkLoss * (this.maxOverrideFlow + 1 - usedOverrideFlow);
  }

  return -1;
};

