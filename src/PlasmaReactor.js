function PlasmaReactor (plasmaFlow, numberOfInjectors) {
  this.plasmaFlow = plasmaFlow;
  this.numberOfInjectors = numberOfInjectors;
  this.injectors = this.initializeInjectors();
}

PlasmaReactor.prototype.initializeInjectors = function () {
  var injectors = new Array();

  for (var i = 0; i < this.numberOfInjectors; i++) {
    injectors.push(new Injector(maxFlow, maxOverrideFlow, overrideSecondsWorkLoss, 0));
  }

  return injectors;
};

PlasmaReactor.prototype.getTotalDamage = function (injectorsDamage) {
  var totalDamage = 0;

  for (var i = 0; i < injectorsDamage.length; i++) {
    totalDamage += injectorsDamage[i];
    if (injectorsDamage[i] == 100) {
      totalDamage -= 20;
    }
  }

  return totalDamage;
};

PlasmaReactor.prototype.getAvailableInjectors = function (injectorsDamage) {
  var availableInjectors = this.numberOfInjectors;

  for (var i = 0; i < injectorsDamage.length; i++) {
    if (injectorsDamage[i] === 100) {
      availableInjectors -= 1;
    }
  }

  return availableInjectors;
};

PlasmaReactor.prototype.getInjectorDamageCompensation = function (injectorsDamage, availableInjectors) {
  return this.getTotalDamage(injectorsDamage) / availableInjectors;
};

PlasmaReactor.prototype.updateSpeedPercentage = function (injectorsOptimizedFlow) {
  var flow = 0;
  injectorsOptimizedFlow.forEach(function (injector) {
    flow += injector[0];
  });
  this.speedOfLightPercentage = flow * 100 / plasmaFlow;
};

PlasmaReactor.prototype.updateWorkingTime = function (injectorsOptimizedFlow) {
  var workingTime = injectorsOptimizedFlow[0][1];
  injectorsOptimizedFlow.forEach(function (injector) {
    if (workingTime > injector[1] && injector[1] !== -2) {
      workingTime = injector[1];
    }
  });
  this.workingTime = workingTime;
};

PlasmaReactor.prototype.getInjectorsFlow = function (speedOfLightPercentage, injectorsDamage) {
  // Validate injectors damage
  if (Array.isArray(injectorsDamage) && injectorsDamage.length !== this.numberOfInjectors) {
    throw new Error("[PlasmaReactor::getInjectorsFlow] Error on injectors damage array");
  }
  // Calculate the optimized flow and the maximum working time for each injector
  var availableInjectors = this.getAvailableInjectors(injectorsDamage);
  var injectorDamageCompensation = this.getInjectorDamageCompensation(injectorsDamage, availableInjectors);
  var speedLimit = 100 - speedOfLightPercentage;
  var injectorsOptimizedFlow = new Array();

  for (var i = 0; i < this.numberOfInjectors; i++) {
    this.injectors[i].damage = injectorsDamage[i];

    var workingTime = -1;
    var optimizedInjectorFlow = this.injectors[i].damage === 100 ? 0 : this.injectors[i].plasmaFlow() + injectorDamageCompensation - speedLimit;

    if (optimizedInjectorFlow > this.injectors[i].overrideMaxFlow()) {
      throw new Error("[PlasmaReactor::getInjectorsFlow] Unable to comply");
    }

    workingTime = this.injectors[i].overrideMaxTime(optimizedInjectorFlow);

    injectorsOptimizedFlow.push([optimizedInjectorFlow, workingTime]);
  }

  this.updateSpeedPercentage(injectorsOptimizedFlow);

  this.updateWorkingTime(injectorsOptimizedFlow);

  return injectorsOptimizedFlow;
};