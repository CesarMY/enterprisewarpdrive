// Injector default configuration values
const maxFlow = 100;
const maxOverrideFlow = 99;
const overrideSecondsWorkLoss = 60;
// Plasma reactor default configuration values
const plasmaFlow = 300;
const numberOfInjectors = 3;
